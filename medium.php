<?php 
?>
<h1>Getting all the users from my Followers list</h1>
<?php 


	$medium = "https://medium.com";

	echo 'Retrieving user ID...';
	$url = $medium.'/@membrana?format=json';
    $data = @file_get_contents($url);

    $data_res = str_replace('])}while(1);</x>', '', $data);
    $data_res = json_decode($data_res);
    

    $user_id = $data_res->payload->user->userId;
    $user_name = $data_res->payload->user->username;
    echo '<br> User id: '.$user_id.'...     Username: '.$user_name.';';
    echo '<br><br>Retrieving users from Followers...<br>';
    $next_id = false;
    $next_id_str = '';
    $followings = [];

    while (true) {
    	if ($next_id != false) {
    		$url = $medium . '/_/api/users/' . $user_id
                  . '/followers?limit=8&to=' . $next_id_str;
    	}
    	else{
    		$url = $medium . '/_/api/users/' . $user_id
                  . '/followers';
    	}
    	$data = @file_get_contents($url);

	    $data_res = str_replace('])}while(1);</x>', '', $data);
	    $data_res = json_decode($data_res);
	    $payload = $data_res->payload->value;

	    foreach ($payload as $user) {
	    	$followings[] = $user->username;
	    }

	    if(!isset($data_res->payload->paging->next->to)){
	    	break;
	    }else{
	    	$next_id_str = $data_res->payload->paging->next->to;
	    	$next_id = true;
	    }

    }
    echo '<br> Followers: <br>';
    foreach ($followings as $key => $value) {
    	echo $key.' => '.$value.'<br>';
    }
    // var_dump($followings);